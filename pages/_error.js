import { Container, Row } from 'reactstrap';
import Navigation from '../components/Navigation';

class Error extends React.Component{
    static getInitialProps({ res, err}){
        const statusCode = res ? res.statusCode : err ? err.statusCode : null;
        return { statusCode};
    }
    render(){
        return(
            <div>
                <Navigation />
                <br/><br/><br/>
                <Container> 
                    <Row>
                        {this.props.statusCode && 
                        `An error ${this.props.statusCode} occurred on server.` }
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Error;