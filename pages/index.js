import withLayout from '../components/Layout';
import React from 'react';
import Typed from 'react-typed';
import { connect } from 'react-redux';

const meQuotes = [
    "Hello, i'm Dorottya Orban.",
    "I'm  a web developer and JS is my lover.", 
    "I'm a writer.",
    "I'm a bookworm.",
    "I'm a traveller.",
    "I'm a boxer.",
    "I'm a driver."
];

class Index extends React.Component{
    static getInitialProps({ store, isServer, pathname, query }) {
        store.dispatch({ type: "FOO", payload: "foooo" }); 
        return { custom: "custom" }; 
    }
    render(){
        return(
        <div className="index-component">
         <div className="background">
            <div className="index-style">
                <Typed
                    loop
                    typeSpeed={50}
                    backSpeed={20}
                    strings={meQuotes}
                    backDelay={1000}
                    loopCount={0}
                    showCursor
                    className="self-typed"
                    cursorChar="|"
                />
            </div>
          </div>
        </div>
        );
    }
}

export default withLayout(connect(state=> state)(Index));