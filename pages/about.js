import withLayout from '../components/Layout';
import Page from '../components/Page';
import { Col } from 'reactstrap';
import { connect } from 'react-redux';

class About extends React.Component{
    static getInitialProps({ store, isServer, pathname, query }) {
        store.dispatch({type: 'IS_AUTHENTICATED', payload: true});

        return { custom: "custom" }; 
    }
    render(){
        return (
            <Page>
                {this.props.authenticated && 'yes'}
                {!this.props.authenticated && 'no'}
                { this.props.custom }
                 <Col xs="12">This is real; This is me;</Col>
            </Page>

        )
    }
}

export default withLayout(connect(state=> state)(About));