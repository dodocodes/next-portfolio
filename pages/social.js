import Page from '../components/Page';
import withLayout from '../components/Layout';

class Social extends React.Component{
    render(){
        return(
            <Page>Find me on social media;</Page>
        )
    }
}

export default withLayout(Social);