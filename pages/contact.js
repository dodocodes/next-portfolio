import Page from '../components/Page';
import withLayout from '../components/Layout';

class Contact extends React.Component{
    render(){
        return(
            <Page>Contact</Page>
        )
    }
}

export default withLayout(Contact);