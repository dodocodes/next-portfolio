import Page from '../components/Page';
import withLayout from '../components/Layout';

class Blog extends React.Component{
    render(){
        return(
            <Page>My blog</Page>
        )
    }
}

export default withLayout(Blog);