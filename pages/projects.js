import Page from '../components/Page';
import withLayout from '../components/Layout';
import CurrentShow from '../components/CurrentShow';
import { Col } from 'reactstrap';

class Projects extends React.Component{
    render(){
        return(
            <Page>
                 <Col xs="12">
                    Projects
                </Col>
                <CurrentShow/>
            </Page>
        )
    }
}

export default withLayout(Projects);