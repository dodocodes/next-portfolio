import App, { Container } from 'next/app';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import withRedux from "next-redux-wrapper";
import 'bootstrap/dist/css/bootstrap.css';
import '../static/styles/index.scss';

const reducer = (state = {foo: ''}, action) => {
  switch (action.type) {
      case 'FOO':
          return {...state, foo: action.payload};
      case 'IS_AUTHENTICATED':
        return {...state, authenticated: action.payload}
      default:
          return state
  }
};

const makeStore = (initialState, options) => {
  return createStore(reducer, initialState);
};

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    ctx.store.dispatch({type: 'FOO', payload: 'foo'});
    ctx.store.dispatch({type: 'IS_AUTHENTICATED', payload: false});
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Container>
         <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}

export default withRedux(makeStore)(MyApp);