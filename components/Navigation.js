import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem
 } from 'reactstrap';
import Link from 'next/link';

class Navigation extends React.Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
    }
    toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
    }
    render(){
        return(
            <Navbar color="faded" light className="fixed-top" expand="md">
                <NavbarBrand href="/">Dorka Orban</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                    <NavItem>
                        <Link href="/about"><a className="nav-link">About</a></Link>
                    </NavItem>
                    <NavItem>
                        <Link href="/projects"><a className="nav-link">Projects</a></Link>
                    </NavItem>
                    <NavItem>
                        <Link href="/blog"><a className="nav-link">Blog</a></Link>
                    </NavItem>
                    <NavItem>
                        <Link href="/contact"><a className="nav-link">Contact</a></Link>
                    </NavItem>
                    <NavItem>
                        <Link href="/social"><a className="nav-link">Social media</a></Link>
                    </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}

export default Navigation;
