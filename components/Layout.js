import Navigation from '../components/Navigation';

const withLayout = WrappedComponent => {
    class HOC extends React.Component {
        static getInitialProps(ctx) {
            if(WrappedComponent.getInitialProps){
                return WrappedComponent.getInitialProps(ctx);
            }
        }
        render(){
            return(
                <div>
                    <Navigation />
                    <WrappedComponent {...this.props} />
                </div>
            )
        }
    }
    return HOC;
}

export default withLayout;