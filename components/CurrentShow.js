import axios from 'axios';
import { TVMAZE_API } from '../consts';
import { Row, Col, Input, Button } from 'reactstrap';
import Link from 'next/link';

class CurrentShow extends React.Component{
    state = {
        shows: false, 
        error: false,
        errorMessage: '',
        searchWord: ''
    }

    searchTvShow = async () => {
        try{
            const response = await axios.get(`http://api.tvmaze.com/singlesearch/shows?q=${this.state.searchWord}`);
            let data = response.data;

            this.setState({
                shows: {
                    'name': data.name,
                    'status': data.status,
                    'rating': data.rating.average,
                    'summary': data.summary
                }
            });

        }catch(err){
            console.error(err);
            this.setState({
                error: true,
                errorMessage: err.message
            })
        }
    }

    render(){
        const { error, errorMessage, searchWord, shows } = this.state;
        return (
            <Col xs="12">
            <Row>
                <Col xs="6">
                    <Input 
                        placeholder="Search for a tv show"
                        value={searchWord}
                        onChange={(e)=> {this.setState({searchWord: e.target.value})}}
                    />
                </Col>
                <Col xs="4">
                    <Button color="primary" onClick={this.searchTvShow}>Search</Button>
                </Col>
            </Row>
            <Col xs="12">
                { error && errorMessage }
                { shows && 
                    <div>
                        <p>Name: {shows.name}</p>
                        <p>Status: {shows.status}</p>
                        <p>Rating: {shows.rating}</p>
                        <div dangerouslySetInnerHTML={{__html: shows.summary}} />
                    </div> }
            </Col>
            </Col>
        )
    }
}

export default CurrentShow;