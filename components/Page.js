import { Container, Row } from 'reactstrap';

const Page = (props) => {
    return(
        <Container className="wrapped-component">
            <Row>
                {props.children}
            </Row>
        </Container> 
    )
}

export default Page;